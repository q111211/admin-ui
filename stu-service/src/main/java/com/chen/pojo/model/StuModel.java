package com.chen.pojo.model;


import cn.hutool.core.date.DateUtil;
import com.chen.pojo.Stu;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StuModel {

    private int id;
    private String stuId;
    private String name;
    private int age;
    //private Sex sexObj;
    private String sex;
    private String dept;
    private String position;
    private String date;
    private String address;
    private String hobby;
    private String introduce;

    /*@Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Sex {
        private int code;
        private String displayName;

        Sex(int code) {
            this.code = code;
            this.displayName = code == 2 ? "女" : "男";
        }
    }*/


    public StuModel(Stu stu) {
        this.id = stu.getId();
        //this.stuId=stu.getStuId();
        this.name = stu.getName();
        this.age = stu.getAge();
        //this.sexObj = new Sex(stu.getSex());
        this.sex=stu.getSex();
        this.dept = stu.getDept();
        this.position = stu.getPosition();
        this.date = DateUtil.format(stu.getDate(), "yyyy-MM-dd HH:mm:ss");
        this.address = stu.getAddress();
        this.hobby = stu.getHobby();
        this.introduce = stu.getIntroduce();
    }


}
