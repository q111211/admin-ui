package com.chen.pojo.beans;


import com.chen.enums.Enums;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommonResult {
    private int code;
    private String msg;
    private Object data;


    public CommonResult(Enums service, Object data) {
        this.code = service.getCode();
        this.msg = service.getMsg();
        this.data = data;
    }




}
