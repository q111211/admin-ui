package com.chen.pojo.models;

import cn.hutool.core.date.DateUtil;
import com.chen.pojo.Users;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserModel {
    private int id;
    private Sex sexObj;
    private String date;
    private String name;
    private int age;
    private String address;
    private String number;
    private String email;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Sex {
        private int code;
        private String displayName;

        Sex(int code) {
            this.code = code;
            this.displayName = code == 2 ?"女":"男";
        }
    }

    public UserModel(Users users) {
        this.id = users.getId();
        this.sexObj = new Sex(users.getSex());
        this.date = DateUtil.format(users.getDate(),"yyyy-MM-dd HH:mm:ss");
        this.name = users.getName();
        this.age = users.getAge();
        this.address = users.getAddress();
        this.number = users.getNumber();
        this.email=users.getEmail();
    }
}
