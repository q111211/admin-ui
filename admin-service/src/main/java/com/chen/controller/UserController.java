package com.chen.controller;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.chen.enums.Enums;
import com.chen.pojo.Users;
import com.chen.pojo.beans.CommonResult;
import com.chen.pojo.beans.UserInfo;
import com.chen.pojo.models.ListQueryUser;
import com.chen.service.Impl.UserServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.catalina.User;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/sys/api/dept")
@Api(value = "UserController", tags = {"用户操作"})
public class UserController {
    @Resource
    private UserServiceImpl userService;


    @ResponseBody
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/deleteUser/{id}", method = RequestMethod.GET)
    public CommonResult deleteUser(@PathVariable String id) {
        if (userService.deleteByPrimaryKey(Integer.parseInt(id)) == 1) {
            return new CommonResult(Enums.SUCCESS, "");
        }
        return new CommonResult(Enums.FAILED_DELETEACCOUNT, "");
    }


    @ResponseBody
    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public CommonResult addUser(@RequestBody Users users) {
        users.setDate(DateUtil.parse(users.getStrDate(), "yyyy-MM-dd HH:mm:ss"));
        if (userService.insert(users) == 1) {
            return new CommonResult(Enums.SUCCESS, "");
        }
        return new CommonResult(Enums.FAILED_CREATEACCOUNT, "");
    }


    @ResponseBody
    @RequestMapping(value = "/updateUser", method = RequestMethod.POST)
    public CommonResult updateUser(@RequestBody Users users) {
//        users.setDate(DateUtil.parse(users.getStrDate(), "yyyy-MM-dd HH:mm:ss"));
        if (userService.updateByPrimaryKey(users) == 1) {
            return new CommonResult(Enums.SUCCESS, "");
        }
        return new CommonResult(Enums.FAILED_UPDATE_USER, "");
    }


    @RequestMapping(value = "/page", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "分页")
    public CommonResult page(@RequestBody ListQueryUser listQueryUser) {

        return userService.getPage(listQueryUser);

    }
}
