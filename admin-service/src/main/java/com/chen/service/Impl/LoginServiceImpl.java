package com.chen.service.Impl;

import cn.hutool.core.lang.ObjectId;
import com.chen.enums.Enums;
import com.chen.mapper.PersonsDao;
import com.chen.pojo.Persons;
import com.chen.pojo.beans.CommonResult;
import com.chen.pojo.beans.UserInfo;
import com.chen.pojo.models.LoginForm;
import com.chen.service.LoginService;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.awt.SystemColor.info;

@Service("loginService")
public class LoginServiceImpl implements LoginService {

    Map<String,Long> redis = new ConcurrentHashMap<>();
    @Resource
    private PersonsDao personsDao;

    @Override
    public CommonResult info(Long keepAlive, String token, Long originalKeepAlive) {
//        if (keepAlive > originalKeepAlive ){
//            return 失效
//        } else {
//
//        }
        UserInfo info = new UserInfo(
                "https://himg.bdimg.com/sys/portrait/item/public.1.cc186001.v5cWpNYBr9dC7t716NhTdw.jpg",
                "我是Java开发人员",
                "陈超",
                new String[]{"admin"}
                );
        return new CommonResult(Enums.SUCCESS, info);

    }

    @Override
    public CommonResult logout(String token, Long originalKeepAlive) {
        return new CommonResult(Enums.SUCCESS, "");
    }

    @Override
    public CommonResult login(LoginForm loginForm) {
        if (ObjectUtils.isEmpty(loginForm)) {
            return new CommonResult(Enums.SUCCESS_USER_LOGINFORM_IS_EMPTY, "");
        }
        Persons persons = personsDao.selectPersonByUserName(loginForm.getUsername());
        if (loginForm.getPassword().equals(persons.getPassword())) {
            // {"code":20000,"data":{"token":"admin-token"}}
            String token = ObjectId.next();
            Map<String, Object> map = new HashMap<>();
            long time = System.currentTimeMillis() + 1000L;
            map.put("token", token);
            map.put("keepAlive", time);
            redis.put(token,time);
            return new CommonResult(Enums.SUCCESS, map);
        } else {
            return new CommonResult(Enums.SUCCESS_USER_ACCOUNTORPASSWORDISERROR, "");
        }
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return personsDao.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Persons record) {
        return personsDao.insert(record);
    }

    @Override
    public int insertSelective(Persons record) {
        return personsDao.insertSelective(record);
    }

    @Override
    public Persons selectByPrimaryKey(Integer id) {
        return personsDao.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Persons record) {
        return personsDao.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Persons record) {
        return personsDao.updateByPrimaryKey(record);
    }
}
