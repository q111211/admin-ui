-- auto-generated definition
create table stu
(
  id        int auto_increment
  comment '主ID'
    primary key,
  stu_id    varchar(255) null
  comment '学生学号',
  name      varchar(255) null
  comment '学生姓名',
  age       int          null
  comment '学生年龄',
  sex       varchar(255) null
  comment '学生性别',
  dept      varchar(255) null
  comment '所在系',
  position  varchar(255) null
  comment '学生职位',
  date      datetime     null
  comment '入学时间',
  address   varchar(255) null
  comment '家庭住址',
  hobby     varchar(255) null
  comment '兴趣爱好',
  introduce varchar(255) null
  comment '个性签名'
);

-- auto-generated definition
create table user
(
  id       int auto_increment
  comment '主键id'
    primary key,
  username varchar(255) null
  comment '用户名',
  password varchar(255) null
  comment '密码'
);

