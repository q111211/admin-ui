package com.chen.service;

import com.chen.pojo.Stu;
import com.chen.pojo.beans.CommonResult;
import com.chen.pojo.model.ListQueryUser;

public interface StuService {
    int deleteByPrimaryKey(Integer id);

    int insert(Stu record);

    int insertSelective(Stu record);

    Stu selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Stu record);

    int updateByPrimaryKey(Stu record);

    CommonResult getPage(ListQueryUser listQueryUser);
}
