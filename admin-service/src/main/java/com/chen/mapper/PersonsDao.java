package com.chen.mapper;

import com.chen.pojo.Persons;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


@Mapper
public interface PersonsDao {
    int deleteByPrimaryKey(Integer id);

    int insert(Persons record);

    int insertSelective(Persons record);

    Persons selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Persons record);

    int updateByPrimaryKey(Persons record);

    Persons selectPersonByUserName(@Param("userName") String userName);
}
