package com.chen.pojo.models;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "用户分页模板")
public class ListQueryUser {
    private String name;
    private int sex;
    private int age;
    private String number;
    private String email;
    private int currentPage;
    private int pageSize;
}
