package com.chen.service;

import com.chen.pojo.User;
import com.chen.pojo.beans.CommonResult;
import com.chen.pojo.model.LoginForm;
import org.apache.ibatis.annotations.Param;

public interface LoginService {

    CommonResult info(Long keepAlive, String token, Long originalKeepAlive);
    CommonResult logout(String token,Long originalKeepAlive);//退出

    CommonResult login(LoginForm loginForm);//登录接口

    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);



}
