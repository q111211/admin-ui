import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'
const KeepAlive='KeepAlive'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function getKeepAlive() {
  return Cookies.get(KeepAlive)
}

export function setKeepAlive(keep) {
  return Cookies.set(KeepAlive, keep)
}

export function removeKeepAlive() {
  return Cookies.remove(KeepAlive)
}
