package com.chen.controller;


import com.chen.pojo.beans.CommonResult;
import com.chen.pojo.model.LoginForm;
import com.chen.service.Impl.LoginServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/sys/api/user")
@Api(value = "UserController", tags = {"用户接口"})
public class LoginController {

    private static Logger log = LoggerFactory.getLogger(LoginController.class);
    @Resource
    private HttpServletRequest request;
    @Resource
    private LoginServiceImpl loginService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "登录")
    public CommonResult userLogin(@RequestBody LoginForm loginForm) {
        return  loginService.login(loginForm);

    }


    @RequestMapping(value = "/info/{t}", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "用户基础信息")
    public CommonResult getInfo(@PathVariable String t) {
        log.info("UserController-info: timestamp = " + t);
        String token = "";
        Long originalKeepAlive = null;
        Long timestamp = null;//时间戳
        try {
            timestamp = Long.parseLong(t);
            token = request.getHeader("token");
            originalKeepAlive = Long.valueOf(request.getHeader("keepAlive"));
        }catch (Exception e) {
            log.error("[info] get info is error {}",e.getMessage());
        }
        log.info("UserController-info: token = " + token);
        log.info("UserController-info: originalKeepAlive = " + originalKeepAlive);
        return loginService.info(timestamp, token, originalKeepAlive);

    }

    @ResponseBody
    @RequestMapping(value = "/logout{t}",method = RequestMethod.POST)
    @ApiOperation("用户退出")
    public CommonResult getLogOut(@PathVariable String t)
    {
        String token = "";
        Long originalKeepAlive =null ;
        try {
            token = request.getHeader("token");
            originalKeepAlive = Long.valueOf(request.getHeader("keepAlive"));
        } catch (Exception e) {
            log.error("The Request Headers or params is null.");
        }
        log.info("UserController-info: token = " + token);
        log.info("UserController-info: originalKeepAlive = " + originalKeepAlive);
        return loginService.logout(token,originalKeepAlive);

    }
}
