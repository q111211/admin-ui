package com.chen.service;

import com.chen.pojo.Users;
import com.chen.pojo.beans.CommonResult;
import com.chen.pojo.beans.UserInfo;
import com.chen.pojo.models.ListQueryUser;

public interface UserService {
    int deleteByPrimaryKey(Integer id);

    int insert(Users record);

    int insertSelective(Users record);

    Users selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Users record);

    int updateByPrimaryKey(Users record);

    CommonResult getPage(ListQueryUser listQueryUser);
}
