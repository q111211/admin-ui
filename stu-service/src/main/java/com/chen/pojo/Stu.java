package com.chen.pojo;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * stu
 * @author
 */
@Data
@ApiModel(description = "添加")
public class Stu implements Serializable {
    /**
     * 主ID
     */
    private Integer id;

    /**
     * 学生学号
     */
    private String  stuId;

    /**
     * 学生姓名
     */
    private String name;

    /**
     * 学生年龄
     */
    private Integer age;

    /**
     * 学生性别
     */
    private String sex;

    /**
     * 所在系
     */
    private String dept;

    /**
     * 学生职位
     */
    private String position;

    /**
     * 入学时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date date;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String strDate;

    /**
     * 家庭住址
     */
    private String address;

    /**
     * 兴趣爱好
     */
    private String hobby;

    /**
     * 个性签名
     */
    private String introduce;

    private static final long serialVersionUID = 1L;
}
