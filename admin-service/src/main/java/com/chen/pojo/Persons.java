package com.chen.pojo;

import java.io.Serializable;
import lombok.Data;

/**
 * persons
 * @author 
 */
@Data
public class Persons implements Serializable {
    /**
     * 用户id
     */
    private Integer id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    private static final long serialVersionUID = 1L;
}