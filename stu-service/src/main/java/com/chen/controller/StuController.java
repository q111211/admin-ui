package com.chen.controller;


import cn.hutool.core.date.DateUtil;
import com.chen.enums.Enums;
import com.chen.pojo.Stu;
import com.chen.pojo.beans.CommonResult;
import com.chen.pojo.model.ListQueryUser;
import com.chen.service.Impl.StuServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/sys/api/dept")
@Api(value = "StuController", tags = {"用户操作"})
public class StuController {

    @Resource
    private StuServiceImpl stuService;

    @ResponseBody
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/deleteUser/{id}", method = RequestMethod.GET)
    public CommonResult deleteStu(@PathVariable  String id)
    {
        if(stuService.deleteByPrimaryKey(Integer.parseInt(id))==1)
        {
            return new CommonResult(Enums.SUCCESS,"");
        }
        return new CommonResult(Enums.FAILED_DELETEACCOUNT,"");
    }

    @ResponseBody
    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public CommonResult addUser(@RequestBody Stu users) {
        users.setDate(DateUtil.parse(users.getStrDate(), "yyyy-MM-dd HH:mm:ss"));
        if (stuService.insert(users) == 1) {
            return new CommonResult(Enums.SUCCESS, "");
        }
        return new CommonResult(Enums.FAILED_CREATEACCOUNT, "");
    }


    @ResponseBody
    @RequestMapping(value = "/updateUser", method = RequestMethod.POST)
    public CommonResult updateUser(@RequestBody Stu users) {
//        users.setDate(DateUtil.parse(users.getStrDate(), "yyyy-MM-dd HH:mm:ss"));
        if (stuService.updateByPrimaryKey(users) == 1) {
            return new CommonResult(Enums.SUCCESS, "");
        }
        return new CommonResult(Enums.FAILED_UPDATE_USER, "");
    }


    @RequestMapping(value = "/page", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "分页")
    public CommonResult page(@RequestBody ListQueryUser listQueryUser) {

        return stuService.getPage(listQueryUser);

    }


}
