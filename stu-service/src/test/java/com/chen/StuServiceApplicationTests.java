package com.chen;

import cn.hutool.core.lang.ObjectId;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class StuServiceApplicationTests {

    @Test
    void contextLoads() {
    }

    public static void main(String[] args) {
        String token = ObjectId.next();
        System.out.println(token);
    }

}
