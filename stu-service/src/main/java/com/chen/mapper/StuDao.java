package com.chen.mapper;

import com.chen.pojo.Stu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface StuDao {

    int deleteByPrimaryKey(Integer id);

    int insert(Stu record);

    int insertSelective(Stu record);

    Stu selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Stu record);

    int updateByPrimaryKey(Stu record);

    List<Stu> getList(@Param("name") String name,
                      @Param("sex") int sex,
                      @Param("age") int age,
                      @Param("dept") String dept,
                      @Param("address") String address);
}
