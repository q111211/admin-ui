import request from '@/utils/request'
import {getKeepAlive} from "@/utils/auth";

export function login(data) {
  return request({
    url: '/sys/api/user/login',
    method: 'post',
    data
  })
}

export function getUserPage(data) {
  return request({
    url: '/sys/api/dept/page',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  var t = new Date().getTime()
  return request({
    url: '/sys/api/user/info/'+t,
    method: 'get',
    headers: { 'token': token, 'keepAlive': getKeepAlive() }
  })
}

export function logout() {
  return request({
    url: '/sys/api/user/logout',
    method: 'post'
  })
}
export function deleteUser(id) {
  return request({
    url: '/sys/api/dept/deleteUser/'+id,
    method: 'get'
  })

}
export function addUser(data) {
  return request({
    url: '/sys/api/dept/addUser',
    method: 'post',
    data
  })
}
export function updateUser(data) {
  return request({
    url: '/sys/api/dept/updateUser',
    method: 'post',
    data
  })
}
