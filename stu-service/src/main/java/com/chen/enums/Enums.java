package com.chen.enums;

public enum Enums {

    SUCCESS(20000, "请求成功"),
    SUCCESS_USER_RELOGIN_UPDATEPASS(201,"密码已修改，请重新登录"),
    SUCCESS_USER_ACCOUNTORPASSWORDISERROR(204,"账户或者密码错误"),
    SUCCESS_USER_LOGINFORM_IS_EMPTY(204,"登录信息不能为空"),
    SUCCESS_USER_ISNOTEXITS(300,"用户不存在"),
    SUCCESS_USER_ISEXITS(300,"用户存在"),
    FAILED(400, "错误请求"),
    FAILED_NO_PARAMS(400, "无参数"),
    FAILED_CREATEACCOUNT(401, "创建用户失败"),
    FAILED_UPDATE_USER(401, "修改用户基本信息失败"),
    FAILED_DELETEACCOUNT(402, "删除用户失败"),
    Error(101, "服务异常");

    private int c;
    private String m;

    Enums(int c, String m) {
        this.c = c;
        this.m = m;
    }


    public String getMsg() {
        return m;
    }

    public int getCode() {
        return c;
    }

    public void setKeyParamsIsNull(String m) {
        this.m = m;
    }



}
