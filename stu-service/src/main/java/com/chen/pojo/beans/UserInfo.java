package com.chen.pojo.beans;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo {
    private String avatar;
    private String introduction;
    private String name;
    private String[] roles;
}
