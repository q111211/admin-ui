/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const tableRouter = {
  path: '/table',
  component: Layout,
  redirect: '/table/complex-table',

  name: 'Table',
  meta: {
    title: '表格',
    icon: 'table'
  }

}
export default tableRouter
