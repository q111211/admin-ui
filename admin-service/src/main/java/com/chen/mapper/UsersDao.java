package com.chen.mapper;

import com.chen.pojo.Users;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface UsersDao {
    int deleteByPrimaryKey(Integer id);

    int insert(Users record);

    int insertSelective(Users record);

    Users selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Users record);

    int updateByPrimaryKey(Users record);

    List<Users> getList(@Param("name") String name,
                        @Param("sex") int sex,
                        @Param("age") int age,
                        @Param("number") String number,
                        @Param("email") String email);
}
