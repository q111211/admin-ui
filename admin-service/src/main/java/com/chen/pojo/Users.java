package com.chen.pojo;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * users
 * @author
 */
@Data
@ApiModel(description = "添加")
public class Users implements Serializable {
    /**
     * 学号
     */
    private Integer id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 性别
     */
    private Integer sex;

    private String strSex;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 出生日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date date;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String strDate;

    /**
     * 家庭住址
     */
    private String address;

    /**
     * 电话号码
     */
    private String number;

    /**
     * 邮箱
     */
    private String email;

    private static final long serialVersionUID = 1L;
}
