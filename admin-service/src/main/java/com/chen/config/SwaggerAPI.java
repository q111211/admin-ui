package com.chen.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
 * @author: GODV
 * @date: 2020/2/1 22:13
 * @params
 * @return:
 * @@Description: 用于测试模块API
 */
@EnableSwagger2
@Configuration
public class SwaggerAPI {
    @Value("${swagger.enable}")
    private boolean enable;

    @Bean
    public Docket getDocket() {
        ParameterBuilder keepAlive = new ParameterBuilder();
        return new Docket(DocumentationType.SWAGGER_2).
                select().
                apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .enable(enable)
                .apiInfo(getInfo());
    }

    /**
     * 对API文档进行全局配置
     */
    private ApiInfo getInfo() {
        ApiInfo api = new ApiInfoBuilder()
                .title("Personloger-Boot-OA API")
                .version("2.0")
                .termsOfServiceUrl("http://www.personloger.com")
                .license("南京Personloger团队版权所有")
                .description("Personloger-Boot-OA 帮助文档")
                .contact(
                        new Contact(
                                "Personloger-Boot-OA",
                                "http://www.personloger.com",
                                "personloger@163.com"))
                .licenseUrl("http://www.personloger.com")
                .extensions(Collections.emptyList())
                .build();
        return api;
    }
}
