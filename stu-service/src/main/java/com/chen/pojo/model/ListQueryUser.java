package com.chen.pojo.model;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "用户分页模板")
public class ListQueryUser {
    private String name;
    private int sex;
    private int age;
    private String dept;
    private String address;
    private int currentPage;
    private int pageSize;
}
