package com.chen.service;

import com.chen.pojo.Persons;
import com.chen.pojo.beans.CommonResult;
import com.chen.pojo.models.LoginForm;

public interface LoginService {
    CommonResult info(Long keepAlive, String token, Long originalKeepAlive);

    CommonResult logout(String token,Long originalKeepAlive);//退出

    CommonResult login(LoginForm loginForm);

    int deleteByPrimaryKey(Integer id);

    int insert(Persons record);

    int insertSelective(Persons record);

    Persons selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Persons record);

    int updateByPrimaryKey(Persons record);
}
