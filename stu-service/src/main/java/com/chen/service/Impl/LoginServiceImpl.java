package com.chen.service.Impl;


import cn.hutool.core.lang.ObjectId;
import com.chen.enums.Enums;
import com.chen.mapper.UserDao;
import com.chen.pojo.User;
import com.chen.pojo.beans.CommonResult;
import com.chen.pojo.beans.UserInfo;
import com.chen.pojo.model.LoginForm;
import com.chen.service.LoginService;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service("loginService")
public class LoginServiceImpl implements LoginService{


    Map<String,Long> redis = new ConcurrentHashMap<>();
    @Resource
    private UserDao userDao;
    @Override
    public CommonResult info(Long keepAlive, String token, Long originalKeepAlive) {
//        if (keepAlive > originalKeepAlive ){
//            return 失效
//        } else {
//
//        }
        UserInfo info = new UserInfo(
                "https://himg.bdimg.com/sys/portrait/item/public.1.cc186001.v5cWpNYBr9dC7t716NhTdw.jpg",
                "我是Java开发人员",
                "陈超",
                new String[]{"admin"}
        );
        return new CommonResult(Enums.SUCCESS, info);

    }

    @Override
    public CommonResult login(LoginForm loginForm) {
        if (ObjectUtils.isEmpty(loginForm)) {
            return new CommonResult(Enums.SUCCESS_USER_LOGINFORM_IS_EMPTY, "");
        }
        User user = userDao.selectUserByUserName(loginForm.getUsername());
        if (loginForm.getPassword().equals(user.getPassword())) {
            // {"code":20000,"data":{"token":"admin-token"}}
            String token = ObjectId.next();
            Map<String, Object> map = new HashMap<>();
            long time = System.currentTimeMillis() + 1000L;
            map.put("token", token);
            map.put("keepAlive", time);
            redis.put(token,time);
            return new CommonResult(Enums.SUCCESS, map);
        } else {
            return new CommonResult(Enums.SUCCESS_USER_ACCOUNTORPASSWORDISERROR, "");
        }

    }

    @Override
    public CommonResult logout(String token, Long originalKeepAlive) {
        return new CommonResult(Enums.SUCCESS, "");
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return userDao.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(User record) {
        return userDao.insert(record);
    }

    @Override
    public int insertSelective(User record) {
        return userDao.insertSelective(record);
    }

    @Override
    public User selectByPrimaryKey(Integer id) {
        return userDao.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(User record) {
        return userDao.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(User record) {
        return userDao.updateByPrimaryKey(record);
    }




}
