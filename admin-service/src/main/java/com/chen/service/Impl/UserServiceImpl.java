package com.chen.service.Impl;


import com.chen.enums.Enums;
import com.chen.mapper.UsersDao;
import com.chen.pojo.Users;
import com.chen.pojo.beans.CommonResult;
import com.chen.pojo.beans.UserInfo;
import com.chen.pojo.models.ListQueryUser;
import com.chen.pojo.models.UserModel;
import com.chen.service.UserService;
import com.chen.utils.PageUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Resource
    private UsersDao usersDao;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return usersDao.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Users record) {
        return usersDao.insert(record);
    }

    @Override
    public int insertSelective(Users record) {
        return usersDao.insertSelective(record);
    }

    @Override
    public Users selectByPrimaryKey(Integer id) {
        return usersDao.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Users record) {
        return usersDao.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Users record) {
        return usersDao.updateByPrimaryKey(record);
    }

    @Override
    public CommonResult getPage(ListQueryUser listQueryUser) {
        List<Users> list = usersDao.getList(listQueryUser.getName(),
                listQueryUser.getSex(),
                listQueryUser.getAge(),
                listQueryUser.getNumber(),
                listQueryUser.getEmail());
        PageUtils<UserModel> page = new PageUtils<UserModel>();
        page.initPage(formatUserToUserModel(list),listQueryUser.getCurrentPage(),listQueryUser.getPageSize());

        return new CommonResult(Enums.SUCCESS,page);
    }

    private List<UserModel> formatUserToUserModel(List<Users> list) {
        List<UserModel> models = new ArrayList<>();
        for (Users user : list) {
            UserModel model = new UserModel(user);
            models.add(model);
        }
        return models;
    }


}
